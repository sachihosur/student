package edu.studInfo.edu.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.studInfo.edu.entity.Students;

@Repository
public interface StudentRepository extends JpaRepository<Students, Long> {
	public List<Students> findAll();
}
