package com.studInfo.edu.entity;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Students {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long rollNumber;

	private String firstName;
	private String lastName;
	private String result;

	public Students() {

	}

	public Students(String firstName, String lastName, long rollNumber, String result) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.rollNumber = rollNumber;
		this.result = result;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(int rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.firstName, this.lastName, this.rollNumber, this.result);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Students)) {
			return false;
		}
		Students stud = new Students();
		return Objects.equals(firstName, stud.firstName) && Objects.equals(lastName, stud.lastName)
				&& Objects.equals(rollNumber, stud.rollNumber) && Objects.equals(result, stud.result);
	}

	@Override
	public String toString() {
		return "Student {" + " firstName = " + firstName + ", lastName = " + lastName + ", rollNumber = " + rollNumber
				+ ", result = " + result + "}";
	}
}
