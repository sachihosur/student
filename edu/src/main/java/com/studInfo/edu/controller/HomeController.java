package com.studInfo.edu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.studInfo.edu.entity.Students;

import edu.studInfo.edu.repository.StudentRepository;

@RestController
@RequestMapping(path="students")
public class HomeController {


	@Autowired // This means to get the bean called userRepository	
    // Which is auto-generated by Spring, we will use it to handle the data
	StudentRepository studentRepo;

	public HomeController() {
		
	}
	
	public HomeController(StudentRepository studentRepo) {
		this.studentRepo = studentRepo;
	}

	@GetMapping("/all")
	public List<Students> findAll() {
		return studentRepo.findAll();
	}

	@PostMapping(path="/addStudent") // Map ONLY POST Requests
	  public @ResponseBody String addNewStudent (@RequestParam int rollNumber,
			  @RequestParam String firstName, @RequestParam String lastName
	      , @RequestParam String result) {
	    // @ResponseBody means the returned String is the response, not a view name
	    // @RequestParam means it is a parameter from the GET or POST request

	    Students student = new Students();
	    student.setRollNumber(rollNumber);
	    student.setFirstName(firstName);
	    student.setLastName(lastName);
	    student.setResult(result);
	    studentRepo.save(student);
	    return firstName + " details saved";
	  }
	
//	@PostMapping("/students")
//	  Student newEmployee(@RequestBody Student newStudent) {
//	    return studentRepo.save(newStudent);
//	  }
//
//	@GetMapping("/employees/{id}")
//	  Student one(@PathVariable Long id) throws Exception {
//
//	    return studentRepo.findById(id)
//	      .orElseThrow(() -> new Exception());
//	  }
	
	
}
